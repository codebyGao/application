package com.ictnj.application_api.java_admin;

import com.ictnj.model.common.dtos.ResponseResult;
import com.ictnj.model.java.pojos.Student;

public interface GetInfo {
    public abstract ResponseResult getStudent();
}

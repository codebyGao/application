package com.ictnj.java.admin.controller;


import com.ictnj.application_api.java_admin.GetInfo;
import com.ictnj.java.admin.config.EnrollAdmin;
import com.ictnj.java.admin.config.RegisterUser;
import com.ictnj.java.admin.mapper.StudentMapper;
import com.ictnj.java.admin.service.InterfaceTest;
import com.ictnj.java.admin.service.imp.FabricInterface;
import com.ictnj.model.common.dtos.ResponseResult;
import com.ictnj.model.java.pojos.Student;
import org.hyperledger.fabric.gateway.*;
import org.hyperledger.fabric.gateway.impl.event.BlockEventSource;
import org.hyperledger.fabric.gateway.impl.event.ContractEventImpl;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Properties;
import java.util.function.Consumer;

@RestController
public class GetInformation implements GetInfo {
    @Autowired
    private StudentMapper studentMapper;

    @PostMapping("/test")
    @Override
    public ResponseResult getStudent() {
        try {

            //获取相应参数
            Properties properties = new Properties();
            InputStream inputStream = GetInformation.class.getResourceAsStream("/fabric.config.properties");
            properties.load(inputStream);

            String networkConfigPath = properties.getProperty("networkConfigPath");
            String channelName = properties.getProperty("channelName");
            String contractName = properties.getProperty("contractName");
            //使用org1中的user1初始化一个网关wallet账户用于连接网络
            String certificatePath = properties.getProperty("certificatePath");
            X509Certificate certificate = readX509Certificate(Paths.get(certificatePath));

            String privateKeyPath = properties.getProperty("privateKeyPath");
            PrivateKey privateKey = getPrivateKey(Paths.get(privateKeyPath));

            Wallet wallet = Wallets.newInMemoryWallet();
//            Wallet wallet1 = Wallets.newFileSystemWallet(Paths.get("wallet"));
            wallet.put("user1", Identities.newX509Identity("Org1MSP", certificate, privateKey));

            //根据connection.json 获取Fabric网络连接对象
            Gateway.Builder builder = Gateway.createBuilder()
                    .identity(wallet, "user1")
                    .networkConfig(Paths.get(networkConfigPath));
            //连接网关
            Gateway gateway = builder.connect();
//            获取与网关连接关联的标识。
            Identity identity = gateway.getIdentity();
            //获取通道
            Network network = gateway.getNetwork("mychannel");

//            HFClient hfClient =HFClient.createNewInstance();
            Channel channel = network.getChannel();

            Collection<Peer> peers = channel.getPeers();

            for (Peer peer : peers) {
                System.out.println(peer.getName());

            }

            //获取合约对象
            Contract contract = network.getContract("myChainCode");
            System.out.println(contract);


//            HFClient hfClient = HFClient.createNewInstance();
//            hfClient.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());
//            User user = FabricInterface.getFabricUser4Local("user1","org1","Org1MSP");
//            hfClient.setUserContext(user);
//            Channel channel1 = hfClient.getChannel(channelName);
//
//            System.out.println(channel1.getName());

            /***********************************/
//            初始化账本  无返回值
//            contract.submitTransaction("InitStudent");


            byte[] findOneStudents = contract.createTransaction("findOneStudent").submit("2");
            System.out.println(new String(findOneStudents,StandardCharsets.UTF_8));

            System.out.println("_______________________________");
            // 增加新的资产
            byte[] invokeResult = contract.createTransaction("CreateAsset")
                    .setEndorsingPeers(network.getChannel().getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)))
                    .submit("asset8", "blue", "20", "Klay", "88");
            System.out.println(new String(invokeResult, StandardCharsets.UTF_8));

            //查询更新后的资产
            byte[] queryAllAssetsAfter = contract.evaluateTransaction("GetAllAssets");
            System.out.println("更新资产：" + new String(queryAllAssetsAfter, StandardCharsets.UTF_8));

//            查询单个资产
            byte[] queryOneAsset = contract.createTransaction("ReadAsset")
                    .submit("asset8");
            System.out.println("更新资产：" + new String(queryOneAsset, StandardCharsets.UTF_8));

//            合约的删除资产的操作
            byte[] submit = contract.createTransaction("DeleteAsset").submit("asset8");

//            删除后不返回任何值 ---
            System.out.println(new String(submit, StandardCharsets.UTF_8));
            //查询现有资产
            //            注意更换调用链码的具体函数
            byte[] queryAllAssets = contract.evaluateTransaction("GetAllAssets");
            System.out.println("所有资产：" + new String(queryAllAssets, StandardCharsets.UTF_8));


        } catch (Exception e) {
            e.printStackTrace();
        }
        Student student = studentMapper.selectById(1);
        return ResponseResult.okResult(student);

    }

    private static X509Certificate readX509Certificate(final Path certificatePath) throws
            IOException, CertificateException {
        try (Reader certificateReader = Files.newBufferedReader(certificatePath, StandardCharsets.UTF_8)) {
            return Identities.readX509Certificate(certificateReader);
        }
    }

    private static PrivateKey getPrivateKey(final Path privateKeyPath) throws IOException, InvalidKeyException {
        try (Reader privateKeyReader = Files.newBufferedReader(privateKeyPath, StandardCharsets.UTF_8)) {
            return Identities.readPrivateKey(privateKeyReader);
        }
    }

    @Autowired
    private EnrollAdmin enrollAdmin;
    @Autowired
    private RegisterUser registerUser;

    @PostMapping("/get")
    public void getConnection() {
        try {
            enrollAdmin.enrollAdmin();
            registerUser.registerUser();
        } catch (Exception e) {
            System.err.println(e);
        }

        try (Gateway gateway = connect()) {
            // get the network and contract
            Network network = gateway.getNetwork("mychannel");
            Contract contract = network.getContract("basic");

            contract.submitTransaction("InitLedger");

            // 增加新的资产
            byte[] invokeResult = contract.createTransaction("CreateAsset")
                    .setEndorsingPeers(network.getChannel().getPeers(EnumSet.of(Peer.PeerRole.ENDORSING_PEER)))
                    .submit("asset8", "blue", "20", "Klay", "88");
            System.out.println(new String(invokeResult, StandardCharsets.UTF_8));

            //查询更新后的资产
            byte[] queryAllAssetsAfter = contract.evaluateTransaction("GetAllAssets");
            System.out.println("更新资产：" + new String(queryAllAssetsAfter, StandardCharsets.UTF_8));

//            查询单个资产
            byte[] queryOneAsset = contract.createTransaction("ReadAsset")
                    .submit("asset8");
            System.out.println("更新资产：" + new String(queryOneAsset, StandardCharsets.UTF_8));

//            合约的删除资产的操作
            byte[] submit = contract.createTransaction("DeleteAsset").submit("asset8");

//            删除后不返回任何值 ---
            System.out.println(new String(submit, StandardCharsets.UTF_8));
            //查询现有资产
            //            注意更换调用链码的具体函数
            byte[] queryAllAssets = contract.evaluateTransaction("GetAllAssets");
            System.out.println("所有资产：" + new String(queryAllAssets, StandardCharsets.UTF_8));

            System.out.println(contract);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Gateway connect() throws Exception {
        // Load a file system based wallet for managing identities.
        Path walletPath = Paths.get("wallet");
        Wallet wallet = Wallets.newFileSystemWallet(walletPath);
        // load a CCP
        Path networkConfigPath = Paths.get("application_java_admin/src/main/resources/crypto-config/peerOrganizations/org1.example.com/connection-org1.yaml");
        Gateway.Builder builder = Gateway.createBuilder();
        builder.identity(wallet, "appUser").networkConfig(networkConfigPath).discovery(true);
        return builder.connect();
    }
}

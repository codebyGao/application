package com.ictnj.java.admin.function;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

/**
 * 学生信息
 */
@DataType
@Data
@NoArgsConstructor
@AllArgsConstructor
public  class Information {

    @Property()
    private  Integer id;
    @Property()
    private  String name;
    @Property()
    private  Integer age;
    @Property()
    private  String gender;
    @Property()
    private  String classes;



}

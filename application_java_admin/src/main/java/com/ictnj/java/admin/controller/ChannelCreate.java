package com.ictnj.java.admin.controller;

import com.ictnj.java.admin.function.CreateChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChannelCreate {

    @Autowired
    private CreateChannel createChannel;

    @PostMapping("channel")
    public void getChannel() {

        createChannel.createChannel();

    }
}

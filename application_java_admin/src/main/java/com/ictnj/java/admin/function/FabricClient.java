package com.ictnj.java.admin.function;


import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.InstallProposalRequest;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.User;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;

/**
 * Wrapper class for HFClient.
 *
 * @author Balaji Kadambi
 */

public class FabricClient {

    private HFClient instance;

    /**
     * Return an instance of HFClient.
     *
     * @return
     */
    public HFClient getInstance() {
        return instance;
    }

    /**
     * Constructor
     *
     * @param context
     * @throws CryptoException
     * @throws InvalidArgumentException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public FabricClient(User context) throws CryptoException, InvalidArgumentException, IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        // setup the client
        instance = HFClient.createNewInstance();
        instance.setCryptoSuite(cryptoSuite);
        instance.setUserContext(context);
    }

//    /**
//     * Create a channel client.
//     *
//     * @param name
//     * @return
//     * @throws InvalidArgumentException
//     */
//    public ChannelClient createChannelClient(String name) throws InvalidArgumentException {
//        Channel channel = instance.newChannel(name);
//        ChannelClient client = new ChannelClient(name, channel, this);
//        return client;
//    }


}



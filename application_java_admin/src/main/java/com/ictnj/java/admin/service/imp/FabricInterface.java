package com.ictnj.java.admin.service.imp;

import com.ictnj.java.admin.service.InterfaceTest;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.hyperledger.fabric.gateway.Identities;
import org.hyperledger.fabric.gateway.Wallet;
import org.hyperledger.fabric.gateway.Wallets;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.identity.X509Identity;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;
import org.springframework.stereotype.Service;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.EnumSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * 使用 HFClient 来进行操作
 **/
@Service
public class FabricInterface implements InterfaceTest {
    @Override
    public void getConnection() throws Exception {
        System.out.println("counter app");

//        创建User实例
        User fabricUser4Local = getFabricUser4Local("user1", "org1", "Org1MSP");

        HFClient org1Client = HFClient.createNewInstance();

        org1Client.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());

        org1Client.setUserContext(fabricUser4Local);

        Channel channel1 = org1Client.newChannel("channel1");

        Peer peer1 = org1Client.newPeer("peer0", "grpcs://192.168.200.130:7051");

        channel1.addPeer(peer1);

        Orderer newOrderer = org1Client.newOrderer("orderer1", "grpcs://192.168.200.130:7050");
        channel1.addOrderer(newOrderer);

        channel1.initialize();


//        ChannelConfiguration channelConfiguration = new ChannelConfiguration(new File("application_java_admin/src/main/resources/channel1.tx"));
//
//        Orderer orderer = org1Client.newOrderer("orderer", "grpcs://192.168.200.130:7050");
//
//        Channel mychannel = org1Client.newChannel("channel1", orderer, channelConfiguration, org1Client.getChannelConfigurationSignature(channelConfiguration, fabricUser4Local));
//
//        Peer peer = org1Client.newPeer("peer0", "grpcs://192.168.200.130:7051");
//
//        mychannel.joinPeer(peer, Channel.PeerOptions.createPeerOptions().setPeerRoles(EnumSet.of(Peer.PeerRole.ENDORSING_PEER, Peer.PeerRole.LEDGER_QUERY, Peer.PeerRole.CHAINCODE_QUERY, Peer.PeerRole.EVENT_SOURCE)));
//        try {
//            mychannel.initialize();
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.exit(0);
//        }
//
//        BlockchainInfo blockchainInfo = mychannel.queryBlockchainInfo(peer);
//        System.out.println("the current ledger blocks height:" + blockchainInfo.getHeight() + " ");


    }

    public static User getUser(String username, String org, String orgId, Enrollment enrollment) {
        FabricUserImpl user = new FabricUserImpl(username, org);
        user.setMspId(orgId);
        user.setEnrollment(enrollment);
        return user;
    }

    /**
     * 根据cryptogen模块生成的账号创建fabric账号
     *
     * @param username
     * @param org
     * @param orgId
     * @return
     */
    public static User getFabricUser4Local(String username, String org, String orgId) {
        FabricUserImpl user = new FabricUserImpl(username, org);
        user.setMspId(orgId);

        try {
            String certificate = new String(IOUtils.toByteArray(new FileInputStream("application_java_admin/src/main/resources/crypto-config/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/cacerts/ca.org1.example.com-cert.pem")));//将生成的fabricTest/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp/admincerts/Admin@org1.example.com-cert.pem保存到本地的某个目录下
            File privateKeyFile = new File("application_java_admin/src/main/resources/crypto-config/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/keystore/priv_sk");//将生成的fabricTest/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp/keystore/****_sk文件保存到本地的某个目录下
            PrivateKey pk = Identities.readPrivateKey(Files.newBufferedReader(Paths.get(privateKeyFile.getPath())));
            EnrollmentImpl enrollement = new EnrollmentImpl(pk, certificate);
            user.setEnrollment(enrollement);
        } catch (IOException | InvalidKeyException e) {
            e.printStackTrace();
        }

        return user;
    }

    public static PrivateKey getPrivateKeyFromBytes(byte[] data) throws IOException {
        final Reader pemReader = new StringReader(new String(data));
        final PrivateKeyInfo pemPair;
        PEMParser pemParse = new PEMParser(pemReader);
        pemPair = (PrivateKeyInfo) pemParse.readObject();
        PrivateKey pk = new JcaPEMKeyConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME).getPrivateKey(pemPair);
        pemParse.close();
        return pk;
    }

    static final class EnrollmentImpl implements Enrollment, Serializable {

        private static final long serialVersionUID = 1L;
        private final PrivateKey privateKey;
        private final String certificate;

        public EnrollmentImpl(PrivateKey pk, String c) {
            this.privateKey = pk;
            this.certificate = c;
        }

        @Override
        public PrivateKey getKey() {
            return privateKey;
        }

        @Override
        public String getCert() {
            return certificate;
        }

    }

    static final class FabricUserImpl implements User, Serializable {

        private static final long serialVersionUID = 1000L;
        private String name;
        private Set<String> roles;
        private String account;
        private String affiliation;
        private String organization;
        private String enrollmentSecret;
        Enrollment enrollment = null;

        public FabricUserImpl(String name, String org) {
            this.name = name;
            this.organization = org;
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public Set<String> getRoles() {
            return this.roles;
        }

        public void setRoles(Set<String> roles) {
            this.roles = roles;
        }

        @Override
        public String getAccount() {
            return this.account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        @Override
        public String getAffiliation() {
            return this.affiliation;
        }

        public void setAffiliation(String af) {
            this.affiliation = af;
        }

        @Override
        public Enrollment getEnrollment() {
            return this.enrollment;
        }

        public boolean isEnrolled() {
            return this.enrollment != null;
        }

        public String getEnrollmentSecret() {
            return this.enrollmentSecret;
        }

        public void setEnrollmentSecret(String es) {
            this.enrollmentSecret = es;
        }

        public void setEnrollment(Enrollment e) {
            this.enrollment = e;
        }

        String mspId;

        @Override
        public String getMspId() {
            return this.mspId;
        }

        public void setMspId(String mspId) {
            this.mspId = mspId;
        }
    }

}

package com.ictnj.java.admin.function;

import com.ictnj.java.admin.config.TestConfig;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.Peer.PeerRole;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.springframework.stereotype.Service;
import org.hyperledger.fabric.sdk.TransactionRequest.Type;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.String.format;
import static org.junit.Assert.*;

@Service
public class CreateChannel {

    public void createChannel() {

        try {
            UserContextImp org1Admin = new UserContextImp();
            String pkPath = "application_java_admin/src/main/resources/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/keystore";
            File pkFile1 = new File(pkPath);
            File[] pkFiles1 = pkFile1.listFiles();
            String certPath = "application_java_admin/src/main/resources/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/signcerts";
            File certFile1 = new File(certPath);
            File[] certFiles1 = certFile1.listFiles();
            Enrollment enrollment = getEnrollment(pkPath, pkFiles1[0].getName(), certPath, certFiles1[0].getName());
            org1Admin.setEnrollment(enrollment);
            org1Admin.setName("admin");
            org1Admin.setMspId("Org1MSP");
            org1Admin.setAffiliation("org1");

            UserContextImp org2Admin = new UserContextImp();
            String pkPath2 = "application_java_admin/src/main/resources/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp/keystore";
            File pkFile2 = new File(pkPath2);
            File[] pkFiles2 = pkFile2.listFiles();
            String certPath2 = "application_java_admin/src/main/resources/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp/signcerts";
            File certFile2 = new File(certPath2);
            File[] certFiles2 = certFile2.listFiles();
            Enrollment enrollment1 = getEnrollment(pkPath2, pkFiles2[0].getName(), certPath2, certFiles2[0].getName());
            org2Admin.setEnrollment(enrollment1);
            org2Admin.setName("user2");
            org2Admin.setMspId("Org2MSP");
            org2Admin.setAffiliation("org2");

            FabricClient fabricClient = new FabricClient(org1Admin);
//            Properties orderPro = new Properties();
            Properties orderPro = loadTLSFile(TestConfig.ORDERER_PEMFILE, TestConfig.ORDERER_CERT, TestConfig.ORDERER_KEY, TestConfig.ORDER_NAME, "OrdererMSP");
            Orderer orderer = fabricClient.getInstance().newOrderer("orderer.example.com", "grpcs://orderer.example.com:7050", orderPro);

            ChannelConfiguration channelConfiguration = new ChannelConfiguration(new File("application_java_admin/src/main/resources/channel1.tx"));

            byte[] channelConfigurationSignatures = fabricClient.getInstance().getChannelConfigurationSignature(channelConfiguration, org1Admin);
            Channel channel1 = fabricClient.getInstance().newChannel("channel1", orderer, channelConfiguration, channelConfigurationSignatures);
//            Channel channel1 = fabricClient.getInstance().newChannel("mychannel");

            for (Peer peer : channel1.getPeers()) {
                System.out.println(peer.getName());
            }
            Properties peer1Pro = loadTLSFile(TestConfig.PEER1_PEMFILE, TestConfig.PEER1_CERT, TestConfig.PEER1_KEY, TestConfig.PEER1_NAME, "Org1MSP");
            Peer peer0_Org1 = fabricClient.getInstance().newPeer(TestConfig.PEER1_NAME, "grpcs://192.168.127.131:7051", peer1Pro);
//            Peer peer1_Org1 = fabricClient.getInstance().newPeer("peer1.org1.example.com", "grpc://192.168.127.130:7056",
//                    loadTLSFile("",""));
            Properties peer2Pro = loadTLSFile(TestConfig.PEER2_PEMFILE, TestConfig.PEER2_CERT, TestConfig.PEER2_KEY, TestConfig.PEER2_NAME, "Org2MSP");
            Peer peer0_Org2 = fabricClient.getInstance().newPeer(TestConfig.PEER2_NAME, "grpcs://192.168.127.131:9051", peer2Pro);
//            Peer peer1_Org2 = fabricClient.getInstance().newPeer("peer1.org2.example.com", "grpc://192.168.127.130:9056");

//            channel1.joinPeer(peer0_Org1, Channel.PeerOptions.createPeerOptions().setPeerRoles(EnumSet.of(Peer.PeerRole.SERVICE_DISCOVERY, Peer.PeerRole.LEDGER_QUERY, Peer.PeerRole.EVENT_SOURCE, Peer.PeerRole.CHAINCODE_QUERY)));
//            channel1.joinPeer(peer0_Org2, Channel.PeerOptions.createPeerOptions().setPeerRoles(EnumSet.of(Peer.PeerRole.SERVICE_DISCOVERY, Peer.PeerRole.LEDGER_QUERY, Peer.PeerRole.EVENT_SOURCE, Peer.PeerRole.CHAINCODE_QUERY)));

            channel1.joinPeer(peer0_Org1, Channel.PeerOptions.createPeerOptions().setPeerRoles(EnumSet.of(PeerRole.ENDORSING_PEER, PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY, PeerRole.EVENT_SOURCE)));
            channel1.addOrderer(orderer);

            assertFalse(channel1.getPeers(EnumSet.of(Peer.PeerRole.EVENT_SOURCE)).isEmpty());
            assertFalse(channel1.getPeers(PeerRole.NO_EVENT_SOURCE).isEmpty());


            channel1.initialize();


            Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO, "Channel created " + channel1.getName());
//
            fabricClient.getInstance().setUserContext(org2Admin);
            channel1 = fabricClient.getInstance().getChannel("channel1");
            channel1.joinPeer(peer0_Org2, Channel.PeerOptions.createPeerOptions().setPeerRoles(EnumSet.of(Peer.PeerRole.ENDORSING_PEER, Peer.PeerRole.LEDGER_QUERY, Peer.PeerRole.CHAINCODE_QUERY, Peer.PeerRole.EVENT_SOURCE)));
            Iterator<Peer> iterator = channel1.getPeers().iterator();

            assertFalse(channel1.getPeers(EnumSet.of(Peer.PeerRole.EVENT_SOURCE)).isEmpty());
            assertFalse(channel1.getPeers(PeerRole.NO_EVENT_SOURCE).isEmpty());
            channel1.initialize();

            while (iterator.hasNext()) {
                Peer peer = iterator.next();
                Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO, peer.getName() + " at " + peer.getUrl());
            }

            Collection<Peer> peers = new LinkedList<>();
            peers.add(peer0_Org1);
            peers.add(peer0_Org2);
//            org1中的peer
            Collection<Peer> peerCollection1 = new LinkedList<>();
            peerCollection1.add(peer0_Org1);

//            org2中的peer
            Collection<Peer> peerCollection2 = new LinkedList<>();
            peerCollection2.add(peer0_Org2);

//            重新进行管理员的定义
            final Map<String, Object> expectedMap = new HashMap<>();
            expectedMap.put("sequence", 1L);
            expectedMap.put("queryBvalue", "310");

//            链码标签, 可以进行自定义
            String chaincode_label = "chaincode_java";
            LifecycleChaincodePackage chaincode_java = createLifecycleChaincodePackage(
                    chaincode_label,
                    Type.JAVA,
                    "application_java_admin/src/main/java/com/ictnj/java/admin/chaincode",
                    "",
                    null
            );
            final String chaincodeName = "myChainCode";
            HFClient instance1 = fabricClient.getInstance();
            instance1.setUserContext(org1Admin);
            String org1InstallChaincode = lifecycleInstallChaincode(instance1, peerCollection1, chaincode_java);
            assertTrue(org1InstallChaincode.contains("chaincode_java"));
            long sequence = -1L;
            QueryLifecycleQueryChaincodeDefinitionRequest queryLifecycleQueryChaincodeDefinitionRequest = instance1.newQueryLifecycleQueryChaincodeDefinitionRequest();
            queryLifecycleQueryChaincodeDefinitionRequest.setChaincodeName(chaincodeName);
            Collection<LifecycleQueryChaincodeDefinitionProposalResponse> proposalResponses = channel1.lifecycleQueryChaincodeDefinition(queryLifecycleQueryChaincodeDefinitionRequest, peerCollection1);
            for (LifecycleQueryChaincodeDefinitionProposalResponse proposalRespons : proposalResponses) {
                if (proposalRespons.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    sequence = proposalRespons.getSequence() + 1L;
                    break;
                } else {
                    if (404 == proposalRespons.getChaincodeActionResponseStatus()) {
                        sequence = 1;
                        break;
                    }
                }
            }
            if (null != expectedMap) {
                assertEquals(expectedMap.get("sequence"), sequence);
            }


            BlockEvent.TransactionEvent transactionEvent = lifecycleApproveChaincodeDefinitionForMyOrg(instance1, channel1, peerCollection1, sequence, chaincodeName, "v1.0.0", null, null, true, org1InstallChaincode).get(32000L, TimeUnit.SECONDS);
            assertTrue(transactionEvent.isValid());

//            将打包的链码序列化为字节数组
            byte[] asBytes = chaincode_java.getAsBytes();


//            进行org2的操作
            HFClient instance2 = fabricClient.getInstance();
            instance2.setUserContext(org2Admin);
            LifecycleChaincodePackage lifecycleChaincodePackage = LifecycleChaincodePackage.fromBytes(asBytes);

            String org2InstallChaincode = lifecycleInstallChaincode(instance2, peerCollection2, lifecycleChaincodePackage);

            BlockEvent.TransactionEvent transactionEvent1 = lifecycleApproveChaincodeDefinitionForMyOrg(instance2, channel1, peerCollection2, sequence, chaincodeName, "v1.0.0", null, null, true, org2InstallChaincode).get(32000L, TimeUnit.SECONDS);
            assertTrue(transactionEvent1.isValid());

//            使用peer 生命周期链码提交命令将链码定义提交到通道。提交命令还需要由组织管理员提交。
            BlockEvent.TransactionEvent transactionEvent2 = commitChaincodeDefinitionRequest(instance2, channel1, sequence, chaincodeName, "v1.0.0", null, null, true, peers).get(32000L, TimeUnit.SECONDS);

            assertTrue(transactionEvent2.isValid());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    。使用peer 生命周期链码 Approvformyorg命令批准链码定义：
    private CompletableFuture<BlockEvent.TransactionEvent> lifecycleApproveChaincodeDefinitionForMyOrg(HFClient client, Channel channel,
                                                                                                       Collection<Peer> peers, long sequence,
                                                                                                       String chaincodeName, String chaincodeVersion, LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy,
                                                                                                       ChaincodeCollectionConfiguration chaincodeCollectionConfiguration, boolean initRequired, String org1ChaincodePackageID) throws InvalidArgumentException, ProposalException {
        LifecycleApproveChaincodeDefinitionForMyOrgRequest myOrgRequest = client.newLifecycleApproveChaincodeDefinitionForMyOrgRequest();
        myOrgRequest.setChaincodeName(chaincodeName);
        myOrgRequest.setSequence(sequence);
        myOrgRequest.setChaincodeVersion(chaincodeVersion);
        myOrgRequest.setInitRequired(initRequired);

        if (chaincodeCollectionConfiguration != null) {
            myOrgRequest.setChaincodeCollectionConfiguration(chaincodeCollectionConfiguration);
        }
        if (null != chaincodeEndorsementPolicy) {
            myOrgRequest.setChaincodeEndorsementPolicy(chaincodeEndorsementPolicy);
        }
        myOrgRequest.setPackageId(org1ChaincodePackageID);
        Collection<LifecycleApproveChaincodeDefinitionForMyOrgProposalResponse> chaincodeDefinitionForMyOrgProposalResponses = channel.sendLifecycleApproveChaincodeDefinitionForMyOrgProposal(myOrgRequest, peers);
        assertEquals(peers.size(), chaincodeDefinitionForMyOrgProposalResponses.size());
        for (LifecycleApproveChaincodeDefinitionForMyOrgProposalResponse response : chaincodeDefinitionForMyOrgProposalResponses) {
            final Peer peer = response.getPeer();

            assertEquals(format("failure on %s  message is: %s", peer, response.getMessage()), ChaincodeResponse.Status.SUCCESS, response.getStatus());
            assertFalse(peer + " " + response.getMessage(), response.isInvalid());
            assertTrue(format("failure on %s", peer), response.isVerified());
        }

        return channel.sendTransaction(chaincodeDefinitionForMyOrgProposalResponses);

    }

    //。使用peer 生命周期链码 Approvformyorg命令批准链码定义：
    private CompletableFuture<BlockEvent.TransactionEvent> commitChaincodeDefinitionRequest(HFClient client, Channel channel, long definitionSequence, String chaincodeName, String chaincodeVersion,
                                                                                            LifecycleChaincodeEndorsementPolicy chaincodeEndorsementPolicy,
                                                                                            ChaincodeCollectionConfiguration chaincodeCollectionConfiguration,
                                                                                            boolean initRequired, Collection<Peer> endorsingPeers) throws Exception {
        LifecycleCommitChaincodeDefinitionRequest lifecycleCommitChaincodeDefinitionRequest = client.newLifecycleCommitChaincodeDefinitionRequest();

        lifecycleCommitChaincodeDefinitionRequest.setSequence(definitionSequence);
        lifecycleCommitChaincodeDefinitionRequest.setChaincodeName(chaincodeName);
        lifecycleCommitChaincodeDefinitionRequest.setChaincodeVersion(chaincodeVersion);
        if (null != chaincodeEndorsementPolicy) {
            lifecycleCommitChaincodeDefinitionRequest.setChaincodeEndorsementPolicy(chaincodeEndorsementPolicy);
        }
        if (null != chaincodeCollectionConfiguration) {
            lifecycleCommitChaincodeDefinitionRequest.setChaincodeCollectionConfiguration(chaincodeCollectionConfiguration);
        }
        lifecycleCommitChaincodeDefinitionRequest.setInitRequired(initRequired);

        Collection<LifecycleCommitChaincodeDefinitionProposalResponse> lifecycleCommitChaincodeDefinitionProposalResponses = channel.sendLifecycleCommitChaincodeDefinitionProposal(lifecycleCommitChaincodeDefinitionRequest,
                endorsingPeers);

        for (LifecycleCommitChaincodeDefinitionProposalResponse resp : lifecycleCommitChaincodeDefinitionProposalResponses) {

            final Peer peer = resp.getPeer();
            assertEquals(format("%s had unexpected status.", peer.toString()), ChaincodeResponse.Status.SUCCESS, resp.getStatus());
            assertTrue(format("%s not verified.", peer.toString()), resp.isVerified());
        }

        return channel.sendTransaction(lifecycleCommitChaincodeDefinitionProposalResponses);

    }

    //    将链码打包
    private LifecycleChaincodePackage createLifecycleChaincodePackage(String chaincodeLabel, Type chaincodeType, String chaincodeSourceLocation, String chaincodePath, String metadataSource) throws InvalidArgumentException, IOException {
        Path metadataSourcePath = null;
        if (metadataSource != null) {
            metadataSourcePath = Paths.get(metadataSource);
        }
        LifecycleChaincodePackage lifecycleChaincodePackage = LifecycleChaincodePackage.fromSource(chaincodeLabel, Paths.get(chaincodeSourceLocation),
                chaincodeType, chaincodePath, metadataSourcePath);
        assertEquals(chaincodeLabel, lifecycleChaincodePackage.getLabel());
        assertEquals(chaincodeType, lifecycleChaincodePackage.getType());
        assertEquals(chaincodePath, lifecycleChaincodePackage.getPath());

        return lifecycleChaincodePackage;
    }

    //    安装链码
    private String lifecycleInstallChaincode(HFClient client, Collection<Peer> peers, LifecycleChaincodePackage lifecycleChaincodePackage) throws InvalidArgumentException, ProposalException {
        int numInstallProposal = 0;
        numInstallProposal = numInstallProposal + peers.size();
        LifecycleInstallChaincodeRequest installChaincodeRequest = client.newLifecycleInstallChaincodeRequest();
        installChaincodeRequest.setLifecycleChaincodePackage(lifecycleChaincodePackage);

        Collection<LifecycleInstallChaincodeProposalResponse> lifecycleInstallChaincodeProposalResponses = client.sendLifecycleInstallChaincodeRequest(installChaincodeRequest, peers);
        assertNotNull(lifecycleInstallChaincodeProposalResponses);
//        进行是否安装成功的判断
        Collection<ProposalResponse> successful = new LinkedList<>();
        Collection<ProposalResponse> failed = new LinkedList<>();
        String packageID = null;
        for (LifecycleInstallChaincodeProposalResponse response : lifecycleInstallChaincodeProposalResponses) {
            if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                System.out.println("success install chaincode");
                successful.add(response);
                packageID = response.getPackageId();
                assertNotNull(format("Hashcode came back as null from peer ", response.getPeer()), packageID);
            } else {
                failed.add(response);
            }
        }
        if (failed.size() > 0) {
            ProposalResponse fail = failed.iterator().next();
            fail("fail");
        }
        return packageID;

    }

    //    将配置项导入到配置pro中
    private static Properties loadTLSFile(String servicePath, String certPath, String keyPath, String hostName, String Msp) throws IOException {
        Properties properties = new Properties();
//    # 其实只需要一个TLS根证书就可以了，比如TLS相关的秘钥等都是可选的
        properties.put("pemBytes", Files.readAllBytes(Paths.get(servicePath)));
        properties.setProperty("clientCertFile", certPath);
        properties.setProperty("clientKeyFile", keyPath);
        properties.setProperty("sslProvider", "openSSL");
        properties.setProperty("negotiationType", "TLS");
        properties.setProperty("trustServerCertificate", "true");
        properties.setProperty("hostnameOverride", hostName);
        properties.setProperty("ssl-target-name-override", hostName);
        if (hostName.contains("peer")) {
            properties.put("grpc.NettyChannelBuilderOption.maxInboundMessageSize", 9000000);
//            properties.setProperty("org.hyperledger.fabric.sdk.peer.organization_mspid", Msp);

        }
        if (hostName.contains("orderer")) {
            properties.put("grpc.NettyChannelBuilderOption.keepAliveTime", new Object[]{5L, TimeUnit.MINUTES});
            properties.put("grpc.NettyChannelBuilderOption.keepAliveTimeout", new Object[]{8L, TimeUnit.SECONDS});
            properties.put("grpc.NettyChannelBuilderOption.keepAliveWithoutCalls", new Object[]{true});
//            properties.setProperty("org.hyperledger.fabric.sdk.orderer.organization_mspid", Msp);
//
        }
        return properties;
    }

    public static CAEnrollment getEnrollment(String keyFolderPath, String keyFileName, String certFolderPath, String certFileName)
            throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, CryptoException {
        PrivateKey key = null;
        String certificate = null;
        InputStream isKey = null;
        BufferedReader brKey = null;

        try {

            isKey = new FileInputStream(keyFolderPath + File.separator + keyFileName);
            brKey = new BufferedReader(new InputStreamReader(isKey));
            StringBuilder keyBuilder = new StringBuilder();

            for (String line = brKey.readLine(); line != null; line = brKey.readLine()) {
                if (line.indexOf("PRIVATE") == -1) {
                    keyBuilder.append(line);
                }
            }

            certificate = new String(Files.readAllBytes(Paths.get(certFolderPath, certFileName)));

            byte[] encoded = DatatypeConverter.parseBase64Binary(keyBuilder.toString());
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
            KeyFactory kf = KeyFactory.getInstance("EC");
            key = kf.generatePrivate(keySpec);
        } finally {
            isKey.close();
            brKey.close();
        }

        CAEnrollment enrollment = new CAEnrollment(key, certificate);
        return enrollment;
    }


}




package com.ictnj.java.admin;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@SpringBootApplication
@EnableDubbo
@EnableDiscoveryClient
@MapperScan("com.ictnj.java.admin")
public class ApplicationJavaAdmin {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationJavaAdmin.class, args);
    }
}

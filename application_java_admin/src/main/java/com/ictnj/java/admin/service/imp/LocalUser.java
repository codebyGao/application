package com.ictnj.java.admin.service.imp;

import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.User;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.identity.X509Enrollment;
import org.hyperledger.fabric.sdk.security.CryptoPrimitives;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.PrivateKey;
import java.util.Properties;
import java.util.Set;

public class LocalUser implements User {
    private String name;
    private String mspId;
    private Enrollment enrollment;

   LocalUser(String name, String mspId) {
        this.name = name;
        this.mspId = mspId;
    }


    private Enrollment loadFromPemFile(String keyFile, String cetFile) throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException, CryptoException {
        Properties properties = new Properties();
        InputStream resourceAsStream = LocalUser.class.getResourceAsStream("/fabric.config.properties");
        properties.load(resourceAsStream);

        String certificatePath = properties.getProperty(cetFile);
        String privateKeyPath = properties.getProperty(keyFile);
        byte[] keyBytes = Files.readAllBytes(Paths.get(privateKeyPath));
        byte[] cerBytes = Files.readAllBytes(Paths.get(certificatePath));
        CryptoPrimitives suite = new CryptoPrimitives();
        PrivateKey privateKey = suite.bytesToPrivateKey(keyBytes);
        PrivateKey privateKey1 = suite.bytesToPrivateKey(privateKey.getEncoded());
        return new X509Enrollment(privateKey1, new String(cerBytes));
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public Set<String> getRoles() {
        return null;
    }

    @Override
    public String getAccount() {
        return null;
    }

    @Override
    public String getAffiliation() {
        return null;
    }

    @Override
    public Enrollment getEnrollment() {
        return enrollment;
    }

    @Override
    public String getMspId() {
        return mspId;
    }
}

package com.ictnj.java.admin.config;

public class TestConfig {


//    orderer
    public static final String ORDERER_PEMFILE="application_java_admin/src/main/resources/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.crt";

    public static final String ORDERER_CERT="application_java_admin/src/main/resources/crypto-config/ordererOrganizations/example.com/users/Admin@example.com/tls/client.crt";

    public static final String ORDERER_KEY="application_java_admin/src/main/resources/crypto-config/ordererOrganizations/example.com/users/Admin@example.com/tls/client.key";

    public static final String ORDER_NAME="orderer.example.com";
//    peer0.org1

    public static final String PEER1_PEMFILE="application_java_admin/src/main/resources/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.crt";

    public static final String PEER1_CERT="application_java_admin/src/main/resources/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/tls/client.crt";

    public static final String PEER1_KEY="application_java_admin/src/main/resources/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/tls/client.key";

    public static final String PEER1_NAME="peer0.org1.example.com";
//    peer0.org2

    public static final String PEER2_PEMFILE="application_java_admin/src/main/resources/crypto-config/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/server.crt";

    public static final String PEER2_CERT="application_java_admin/src/main/resources/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/tls/client.crt";

    public static final String PEER2_KEY="application_java_admin/src/main/resources/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/tls/client.key";

    public static final String PEER2_NAME="peer0.org2.example.com";



}

package com.ictnj.java.admin.controller;

import com.ictnj.java.admin.service.InterfaceTest;
import com.ictnj.java.admin.service.imp.FabricInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChannelBuild {

    @Autowired
    private FabricInterface interFace;

    @PostMapping("build")
    public void createChannel() throws Exception {
        interFace.getConnection();

    }

}

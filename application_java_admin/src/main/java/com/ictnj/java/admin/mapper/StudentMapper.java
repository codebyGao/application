package com.ictnj.java.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ictnj.model.java.pojos.Student;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentMapper extends BaseMapper<Student> {
}

package com.ictnj.java.admin.function;

import cn.hutool.json.JSONObject;
import com.mysql.cj.xdevapi.JsonArray;
import com.owlike.genson.Genson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.Contract;
import org.hyperledger.fabric.contract.annotation.Info;
import org.hyperledger.fabric.contract.annotation.Transaction;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;

import javax.json.Json;
import java.util.ArrayList;
import java.util.List;

@Contract(
        name = "myChainCode",
        info = @Info(
                title = "My Chaincode",
                version = "0.0.1-SNAPSHOT"
        )
)
public class MyChaincode implements ContractInterface {
    private final Log Log_info = LogFactory.getLog(MyChaincode.class);
    private final Genson genson =new Genson();

    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public void InitStudent(Context ctx) {
        ChaincodeStub chaincodeStub = ctx.getStub();
        increaseStudent(ctx, 1, "高栋栋", 17, "男", "高二");
        increaseStudent(ctx, 2, "芮芳", 17, "女", "高二");
        increaseStudent(ctx, 3, "高文文", 18, "女", "大一");
        increaseStudent(ctx, 4, "高子强", 14, "男", "初三");

    }

    //    增加学生信息
    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public Information increaseStudent(Context ctx, int id, String name, int age, String gender, String classes) {
//        检查是否已经存在相关的信息
        Boolean aBoolean = checkExist(ctx, id);
        if (aBoolean) {
            String errorMessage = String.format("Asset %s already exists", id);
            Log_info.error(errorMessage);
            throw new ChaincodeException(errorMessage, "ASSET_ALREADY_EXISTS");
        }
        ChaincodeStub stub = ctx.getStub();
        Information student = new Information(id, name, age, gender, classes);
        String studentInformation = new JSONObject(student).toString();
        stub.putStringState(String.valueOf(id), studentInformation);
        return student;
    }

    private Boolean checkExist(Context ctx, int id) {
        ChaincodeStub stub = ctx.getStub();
        String stringState = stub.getStringState(String.valueOf(id));
        return (stringState != null && !stringState.isEmpty());
    }

    //    修改学生信息
    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public Information updateStudent(Context ctx, int id, String name, int age, String gender, String classes) {
        ChaincodeStub stub = ctx.getStub();
        Boolean aBoolean = checkExist(ctx, id);
        if (!aBoolean) {
            Log_info.error("The content you want to modify does not exist", new ChaincodeException());
        }
        Information student = new Information(id, name, age, gender, classes);
        String studentJson = new JSONObject(student).toString();
        stub.putStringState(String.valueOf(id), studentJson);
        return student;
    }

    //    删除学生信息
    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public void deleteStudent(Context ctx, int id) {
        ChaincodeStub stub = ctx.getStub();
        Boolean checkExist = checkExist(ctx, id);
        if (!checkExist) {
            Log_info.error("The content you want to modify does not exist", new ChaincodeException());
        }
        stub.delState(String.valueOf(id));
    }

    //    查询学生的信息
    @Transaction(intent = Transaction.TYPE.EVALUATE)
    public String findOneStudent(Context ctx, int id) {
        ChaincodeStub stub = ctx.getStub();
        Boolean checkExist = checkExist(ctx, id);
        if (!checkExist) {
            Log_info.error("The content you want to modify does not exist", new ChaincodeException());
        }
        String stringState = stub.getStringState(String.valueOf(id));
        return new JSONObject(stringState).toString();
    }

    //    查询全部学生信息
    @Transaction(intent = Transaction.TYPE.EVALUATE)
    public String queryAll(Context ctx) {
        ChaincodeStub stub = ctx.getStub();
        List<Information> listInformation = new ArrayList<>();
        QueryResultsIterator<KeyValue> stateByRange = stub.getStateByRange("", "");

        for (KeyValue keyValue : stateByRange) {
            Information student = genson.deserialize(keyValue.getStringValue(), Information.class);
            listInformation.add(student);
        }
        return new JSONObject(listInformation).toString();
    }

}
